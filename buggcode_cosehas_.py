print(SynthDefs)

print(FxList)

Clock.bpm=10



bi >> jbass([2,1,2,1], dur=PDur(1,8), delay=0, amp=0.5).stop()

b1 >> jbass([9,11,8,8,9,11,8,6], dur=PDur(2,8), chop=4, oct=4, delay=0.08).stop()

i1 >> sinepad([2,2,1,1,2,2,1,1], chop=2, delay=0).stop()

i2 >> sinepad([4,4,3.5,3.5,4,4,3.5,3.5], chop=4, delay=0).stop()

b2 >> jbass([9,8,9,8,9,8,9,8,7,6,7,6,7,6,7,8.5], dur=PDur(1,8), chop=0, oct=4).stop()

bs >> jbass([9,11,8,8,9,11,8,6,9,11,8,8,9,11,8,6,7,7,6,6,7,9,6,4,7,7,6,6,7,9,6,8.5], dur=PDur(2,8), chop=4, oct=4, delay=0.08)

a1 >> sinepad([2,2,1,1,2,2,1,1,2,2,1,1,2,2,1,1,0,0,1.5,2,0,0,2,2,0,0,1.5,2,0,0,1.5,1.5], chop=3)

a2 >> sinepad([6,6,5,5,6,6,5,5,6,6,5,5,6,6,5,5,4,4,5,6,4,4,6,6,4,4,5,6,4,4,6,5], chop=3, amp=0.5, echo=1)

a3 >> sinepad([4,4,3.5,3.5,4,4,3.5,3.5,4,4,3.5,3.5,4,4,3.5,3.5], chop=3, delay=0.08, echo=4)

a4 >> sinepad([2,2,2,2,5,5,5,5,2,2,2,2,5,5,4,4,2,2,2,2,5,5,5,5,2,2,2,2,6,6,5,5,2,6,6,5,5,5,5,5,2,2,2,2,6,6,5,5,2,2,2,2,5,5,5,5,2,6,6,6,6,6,5,5], dur=PDur(4,8), chop=3, amp=0.8, echo=4)


a5 >> viola([2,6,6,6,6,5,4,5,2,2,6,6,6,5,4,3.5,2,6,6,6,6,5,4,5,2,2,3.5,4,5,4,3.5,4,2,2,3.5,4,5,5,5,5,5,2,3.5,4,5,4,3.5,4,2,2,3.5,4,5,5,5,5,5,2,3.5,4,5,4,3.5,5], dur=PDur(4,8), echo=2.08) 
 


kt >> play("    X       X       X       X   ", dur=PDur(8,8), sample=0, amp=1.3)

hh >> play("    -  -    -       -  -    -   ", dur=PDur(16,8), sample=2, amp=0.7)


h2 >> play("n   ", amp=0.5)

h3 >> play(" ~  ", sample=1)
   

cj >> play("  % ", sample=0, amp=3).stop()

cx >> play("                         K     K       K      K        K K    KK", sample=2, dur=PDur(8,8), amp=0.3).stop()

c2 >> play("  K ", ample=2, amp=0.7).stop()

h4 >> play("N", amp=0.5, dur=PDur(8,8), sample=4)

tt >> play("E               ", sample=1, amp=0.25)

t1 >> play("   E            ", sample=1, amp=0.07)

t2 >> play("        E       ", sample=1, amp=0.15)

t3 >> play("            E   ", sample=2, amp=1)




Clock.bpm=120

bd >> jbass([0.5,0.5,1,1], chop=2) 

a2 >> ambi([3,2.5,1,1], amp=0.5, chop=2)

ad >> feel([0.5], amp=0.7)

a3 >> feel([0.5,0.5,1,1], dur=PDur(4,8), amp=0.4, chop=1)

a4 >> feel([4.5,4.5,5,5], amp=0.4, chop=1)



k1 >> play("X ", amp=1.4).stop()

h1 >> play(" s")

h2 >> play("ss", dur=PDur(8,8), amp=0.3)

h3 >> play("nn", sample=1, dur=PDur(6,8), amp=0.4)

kt >> play("X  X   XX X ", amp=1.4)


pl >> play(" HH", sample=0, amp=0.8)

h2 >> play("ss")

cj >> play("   o     o      o  o    ", dur=PDur(8,8), sample=3)

c2 >> play("           o  o        o           o   o o o  o ", dur=PDur(8,8), sample=3, amp=0.3)

b3 >> jbass([5], dur=PDur(2,8), chop=2).stop()

i8 >> charm([5], dur=PDur(3,8), chop=3).stop()

i9 >> charm([9], dur=PDur(3,8), chop=3).stop()

bs >> jbass([5,5,5.5,5.5,4,4], chop=2, amp=1.5, oct=4).stop()



b1 >> jbass([0.5,0.5,0.5,1,1,1,2,2,1,1,0.5,0.5], dur=PDur(4,8), amp=1, chop=1)

ii >> charm([5,5,5.5,5.5,5.5,5.5,7,5.5,5], dur=PDur(3,8), oct=4, chop=2)

ip >> charm([9,9,8,8,8,8,5.5,8,9], dur=PDur(3,8), chop=2)

i4 >> charm([12,12,12,12,11,11,12.5,11,12], dur=PDur(3,8), chop=2)

i2 >> charm([14.5,14.5,15,15,15,15,15,14.5,14.5], dur=PDur(3,8), chop=2, amp=0.8)

i3 >> charm([16,16,16,16,16,16,15,16,16], dur=PDur(3,8), amp=0.6).stop()






a3 >> marimba([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,2,2,2,2,2,2,2,2,2], dur=PDur(18,32), chop=2, amp=1.6)

a4 >> marimba([3.5,3.5,3.5,3.5,3.5,2,2,2,2,5,5,5,5,5,4,4,3.5,1], dur=PDur(18,32), chop=2, amp=1.2)

kt >> play("X X X X X X X XX", amp=1.4, dur=PDur(4,8)).stop()

db >> jbass([0.5,-1,0,0,0.5,-1,-1,1,0,1,-1.5,0,1.5,0] + PSine(4)* 2.5, dur=PDur(14,32), scale=Scale.blues, cutoff=linvar([100,8000],50), amp=0.5, fmod=PWhite(-1,1)).stop()

a1 >> marimba([0.5,2] + PSine(5)* 2.5, dur=PDur(18,32), amp=1, scale=Scale.blues)

a2 >> marimba([3.5,5] + PSine(5)* 4.5, dur=PDur(18,32), amp=0.8)


t1 >> play("p  p  p   p p   ", dur=PDur(8,8)).stop()

t2 >> play("     p  p     p      pp       pp     pp pp    p     pp  pp    pp", dur=PDur(8,8), amp=0.2).stop()


h1 >> play(" s").stop()

h2 >> play("s ss", sample=2, dur=PDur(8,8), amp=0.6) 

h4 >> play("ss", dur=PDur(8,8), sample=2, amp=0.2)

h3 >> play("  = ", amp=0.65, dur=PDur(8,8)).stop()






Clock.bpm=120


kt >> play("X ", dur=PDur(4,8))

bs >> sawbass([0,0,0,0,1.5,3,0,0,0,1.5,3,1.5,0,0,0,1.5,3,3.5], dur=[0.805,4], chop=0).stop()

ba >> sawbass([0,0,0,0,0,0,0,0], dur=PDur(1,32), chop=32).stop()

l1 >> sinepad([7,7,7,8.5,7,7,11,9.5,7,7,7,8.5,7,7,7,8.5,7,7,7,8.5,7,11,11,9.5,7,7,7,8.5,7,7,5.5,6,7,7,7,8.5,7,7,5.5,6], dur=PDur(4,8), oct=4, chop=0)

l2 >> sinepad([3,3,3,4,3,3,3,1.5], amp=0.8, dur=PDur(9,16), pan=[1,-1]).stop()

l3 >> sinepad([5.5,4.5,4], amp=0.7, dur=PDur(6,16), pan=[1,-1]).stop()



kt >> play("   X  X ", dur=PDur(8,8))

hh >> play(" s")

h1 >> play("ssss", dur=PDur(8,8)).stop()

h2 >> play("r rr", dur=PDur(8,8), sample=2, amp=0.3).stop()

h3 >> play("f", dur=PDur(2,8), sample=3).stop()

cj >> play("  u    u u     u  u    u   u   u", dur=PDur(8,8)).stop()

c2 >> play("     u    u  u            u  u  ", dur=PDur(8,8), amp=0.5).stop()





vz >> play("w     w     ww            w     ", dur=PDur(8,8), sample=2, amp=0.2).stop()






jb >> jbass([2,2,2,1.5,2,2,2,1.5,2.5,2,2.5,2.5,2,2,3,3,3,2.5,1,1,1,1,1,1,2,2,2.5,2.5,1,1,2,2], oct=4, dur=[0.5,1.5], chop=1, amp=1, delay=(0,1), pan=[1,-1])  

g1 >> gong([5,5,5,5], chop=1, oct=4, pan=[1,-1])

g1 >> gong([5,5.5,8,5,5,5], dur=PSum(3,2), chop=1, oct=4).stop()

gg >> gong([5,5.5,8,5,5,5,5,5.5,8,5,5,5,5,5.5,8,5,5,5,5,5.5,8,9,9,9,11,10,9,8,8,8,11,10,9,8,8,8,5,5.5,8,5,5,5,5,5.5,8,5,5,5], dur=PSum(3,2), chop=0.5, oct=4, amp=1.4).stop()


kt >> play("X    X  X  X    ", dur=PDur(4,8)).stop()

ht >> play("ss", dur=PDur(8,8)).stop()

cc >> play("  u ", dur=PDur(2,8), sample=1).stop()





ba >> jbass([7,7,7,7,7.5,7.5,7,7,7,7,6,6,7,7,7,7,7.5,8.5,7,7,7,7,4,5.5,7,7,7,7,4,8.5,7,7,7,7,4,5.5], dur=[0.5,1.5], oct=4)

s1 >> sinepad([2.8,2.8,2.8,2.8,4,4,2,2,2,2,0.5,0.5], dur=PDur(4,8))

s2 >> sinepad([4,4,4,4,7.5,6.5,5.4,5.4,5.4,5.4,2.2,2.2,4,4,4,4,7.5,6.5,5.4,5.4,4.5,3.5,4,2.2], dur=PDur(6,16), chop=(2))


kt >> play("X    X  X X  X  X    X  X  X X  ", dur=PDur(4,8))

cj >> play("  u ", sample=1, dur=PDur(1,4))

h5 >> play("n   n   n   n   nnnnn   n n n n n   n   n   nn  n   n   nn  nn  ", dur=PDur(16,8))

h6 >> play("s ", dur=PDur(8,8), amp=0.6)








kt >> play("X ",dur=PDur(16,8))


bz >> jbass([2], dur=PDur(2,8), chop=4, amp=0.9).stop()

bs >> jbass([2,2,2,2,2.5,2.5,2.5,2.5], dur=PDur(4,8), chop=2, amp=1.0)



i1 >> feel([2,2,3,5], chop=2)
    
i2 >> feel([4.5,4.5,5,3], chop=2)

i3 >> feel([6,6,6,6,6,6,10,8,6,6,4.5,7], chop=2)


hs >> play("nnnnn nnnnnnn n", dur=PDur(8,8), amp=0.7).stop()

kt >> play("X ", dur=PDur(4,8))

hh >> play(" s", amp=2)

cd >> play("  o ")




    

 














print(SynthDefs)

Clock.bpm=140











bb >> jbass([0,0.5], dur=[8/4], oct=5, chop=4, amp=2)

ls >> lazer([4,4.5], dur=[8/1], chop=16, oct=6)

dj >> pasha([0,0.5], dur=[8/1], chop=32)

dc >> bell([4,5.5,4,4.5], dur=[8/4], chop=8)

zz >> play(" bb", chop=2, amp=0.5, dur=PDur(3,8)).stop()

bm >> play ("V ", dur=PDur(4,8), sample=5)

pl >> play (" n", sample=30)

cj >> play("  o ").stop()

rs >> play(" s", amp=2)

th >> play("nnnn", dur=PDur(8,8))


bm >> play("V ", dur=PDur(16,8))


dk >> play("X ", dur=PDur(8,8))

sb >> razz([0.5,0.5,0.5,0.5,1.5,2,2,3.5,0.5,0.5,0.5,0.5,1.5,2,2,0.5], oct=3, chop=4, amp=3)

s1 >> quin([4.5,4.5,4.5,3.5,1.5,1.5,1.5,2,4.5,4.5,4.5,3.5,1.5,1.5,1.5,0.5], chop=4, amp=0.7)

s2 >> quin([6,6,6,5.5,4.5,4.5,4.5,4.5,6,6,6,5.5,4.5,4.5,4.5,2], chop=4, amp=0.7)

s3 >> quin([8.5,8.5,7,7.5], amp=0.5, dur=PDur(1,8), chop=2).stop()

ls >> lazer([4.5,4.5,4.5,6,5.5,5.5,6,6,4.5,4.5,4.5,6,2,2,5.5,5.5], oct=6, chop=2)

bl >> bell([8.5,7.5,6,5.5,8.5,4.5,3.5,4.5], dur=PDur(1,16), chop=8, amp=0.6).stop()

dk >> play("X         X     X X       X  X  ",dur=PDur(8,8))

dc >> play("    o       o  o    o       o   ",dur=PDur(8,8))


dh >> play("nnnnn nnnnnnn n", dur=PDur(8,8))

hw >> play(" s", amp=2)






Clock.bpm=120

k1 >> play("V      V  V     V      V      V V      V  V     V      V     V  ", dur=PDur(8,8))

k3 >> play("                         VV     ", dur=PDur(8,8), amp=0.5)

c1 >> play("  o ")

c3 >> play("               o              o                o         o      ", dur=PDur(8,8), amp=0.5)

h2 >> play(" %", sample=3, amp=0.3, chop=2)


b1 >> jbass([5], dur=PDur(1,8), chop=8, oct=4).stop()

bs >> jbass([5,2.5,4,5.5,7,7,5,2.5,4,5,5,4], oct=4, chop=1, dur=PDur(3,8), amp=1.3)

a2 >> zap([2,2,4,5,2,2,6,5,2,2,4,5,2,2,0,1.5], dur=PDur(4,8), chop=1, amp=1)

a3 >> zap([5,5,5,5,7,7,7,7,5,5,5,5,4,4,2,2,5,5,5,5,7,7,7,7,5,5,5,5,4,4,3.5,3.5], dur=PDur(8,8), chop=1, amp=0.8)

a4 >> zap([7,7,7,7,8,8,7,7,7,7,6,6,7,7,7,7], dur=PDur(8,8), chop=1, amp=0.8)

mb >> soprano([8], oct=3, amp=0.4)

m2 >> soprano([5], oct=3, amp=0.3)

su >> quin([2], dur=PDur(1,16), chop=16, amp=0.5).stop()

s1 >> lazer([5], dur=PDur(4,8), chop=4, amp=1.4).stop()



kt >> play("V ", sample=3 , amp=0.8)

k2 >> play("              V            V  V               V            V  V               V            V  V               V       V    V  V ", sample=3, dur=PDur(8,8), amp=0.3)

cr >> play("p  p   p  p p   ", sample=0, dur=PDur(8,8), amp=0.7)

c2 >> play("     p  p     p      pp       pp     pp pp    p     pp  pp    pp", dur=PDur(8,8), amp=0.2)

h1 >> play("n nn", sample=0, dur=PDur(8,8), amp=0.4)

h2 >> play(" %", sample=3, amp=0.3, chop=2)

cj >> play("           o  o         o  o  o ", dur=PDur(8,8), amp=0.6)

j2 >> play("                 o        o          o           o    oo o     o", dur=PDur(8,8), amp=0.3)

jc >> play("  o ", sample=4, amp=0.7)


Clock.bpm=136

a1 >> glass(P[var.chords,4], chop=2, amp=7.8).stop()

a2 >> pads([4,1.5,0,1], chop=8, dur=PDur(2,16), amp=0.7)

a3 >> pads([5.5,5.5,7,4], chop=2, dur=PDur(2,16), amp=0.3)

a4 >> pads([4,1.5,0,1], chop=16, dur=PDur(1,16), amp=0.7).stop()

a5 >> pads([5.5,5.5,7,4], chop=4, dur=PDur(1,16), amp=0.3)

kt >> play("X       X X     X       X X     ", dur=PDur(8,8))

cj >> play("  o ", sample=1)

c2 >> play("      o                o                               o   o   o", dur=PDur(8,8), sample=1, amp=0.5)

hh >> play("s ")

h2 >> play(" s", sample=3, amp=0.3)

h3 >> play("ss", dur=PDur(8,8), amp=0.6)

h4 >> play(" s", sample=1)



Clock.bpm=136

a1 >> glass(P[var.chords,4], chop=2, amp=7.8).stop()

a2 >> pads([4,1.5,0,1], chop=8, dur=PDur(2,16), amp=0.7)

a3 >> pads([5.5,5.5,7,4], chop=2, dur=PDur(2,16), amp=0.3)

a4 >> pads([4,1.5,0,1], chop=16, dur=PDur(1,16), amp=0.7).stop()

a5 >> pads([5.5,5.5,7,4], chop=4, dur=PDur(1,16), amp=0.3)

kt >> play("X       X X     X       X X     ", dur=PDur(8,8))

cj >> play("  o ", sample=1)

c2 >> play("      o                o                               o   o   o", dur=PDur(8,8), sample=1, amp=0.5)

hh >> play("s ")

h2 >> play(" s", sample=3, amp=0.3)

h3 >> play("ss", dur=PDur(8,8), amp=0.6)

h4 >> play(" s", sample=1)
