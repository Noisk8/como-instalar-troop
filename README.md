# como instalar troop

Guía para instalar  troop en  [![alt tag](http://icons.iconarchive.com/icons/dakirby309/simply-styled/32/OS-Linux-icon.png)](https://fr.wikipedia.org/wiki/Linux) y [![alt tag](http://icons.iconarchive.com/icons/yootheme/social-bookmark/32/social-windows-button-icon.png)](https://fr.wikipedia.org/wiki/Microsoft_Windows)


## Troop 

[Website](https://github.com/Qirky/Troop)

Troop es una herramienta para hacer livecoding de manera colectiva desde  diferentes ordenadores 


- Instalar foxdot

- Instalar troop 



[![alt tag](http://icons.iconarchive.com/icons/dakirby309/simply-styled/32/OS-Linux-icon.png)](https://fr.wikipedia.org/wiki/Linux)


Este script instala python supercollider y descarga foxdot por pip

Abrir una terminal  y ejecutar los siguientes comandos 

1. Descargar el script 

~~~
wget https://github.com/Noisk8/InstalandoFoxDot-En-linux/blob/master/Debian-Ubuntu/foxdot.sh
~~~

2. Dar permisos de ejecucón al script 

~~~
chmod +x foxdot.sh
~~~

3. Abrir supercollider y dentro de supercollider ejecuta el siguiente comando, este comando espara instalar los synts y samples a super collider

~~~
Quarks.install("FoxDot")
~~~

4. Recompilar la libreria en supercollider

→ Lenguaje → Recompilar lenguaje 

5. Inicia el proceso de foxdot en supercollider 

~~~
Foxdot.start
~~~


## Descargar troop 

1. Descargar vía git el repositorio de troop 

~~~
git clone https://github.com/Qirky/Troop.git
~~~

## 👀 <sub> </sub>

2. Entrar a la carpeta de troop

~~~
cd /ruta de troop/
~~~

3. Lanzar el servidor 

~~~
python run-server.py 
~~~

[![te pide que crees una contraseña pero si presionas entrer no crea ninguna](https://emupedia.net/beta/emuos/assets/images/icons/under-development.svg)](#-view-a-live-demo)

4. 

## 👀 <sub> archivos .py</sub>

[![Contenido con cuidado](https://emupedia.net/beta/emuos/assets/images/icons/under-development.svg)](#-view-a-live-demo)

## 👀 <sub>View a Live Demo</sub>


[![alt tag](http://icons.iconarchive.com/icons/yootheme/social-bookmark/32/social-windows-button-icon.png)](https://fr.wikipedia.org/wiki/Microsoft_Windows)



